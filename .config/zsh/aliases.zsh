##
## Aliases
##

# command
alias c="clear"
alias mtar='tar -zcvf' # mtar <archive_compress>
alias utar='tar -zxvf' # utar <archive_decompress> <file_list>
alias z='zip -r' # z <archive_compress> <file_list>
alias uz='unzip' # uz <archive_decompress> -d <dir>
alias sr='source ~/.config/zsh/env.zsh'
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e" 
alias mkdir="mkdir -p"
alias cat="bat --color always --plain"
alias grep='grep --color=auto'
alias mv='mv -v'
alias cp='cp -vr'
alias rm='rm -vr'
alias grep='grep --color=auto'
alias vim='nvim'
alias quem-v='/usr/bin/qemu-system-x86_64 --version'

# ls
alias ls="exa --color=auto --icons"
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -al'

# git
alias commit="git add . && git commit -m"
alias push="git push"
alias git-rm="git ls-files --deleted -z | xargs -0 git rm"
alias g=git
alias ga='git add'
alias gaa='git add --all'

# cd
alias ..="cd .."
alias cdr="cd ~/repos/"
alias cdgithub="cd ~/repos/github"
alias cdsince69="cd ~/repos/gitlab/since69"
alias cdsandbox="cd ~/repos/gitlab/since69/sandbox"
alias cdsandbox-c="cd ~/repos/gitlab/since69/sandbox/c"
alias cdsandbox-web="cd ~/repos/gitlab/since69/sandbox/web"
alias cdsandbox-dotnet="cd ~/repos/gitlab/since69/sandbox/web/dotnet"
alias cdsfd="cd ~/repos/gitlab/since69/sfd"
alias cdsfd-db="cd ~/repos/gitlab/since69/sfd/db"
alias cdsfd-src="cd ~/repos/gitlab/since69/sfd/src"
alias cdkra="cd ~/repos/gitlab/since69/kra"
alias cdvan="cd ~/repos/gitlab/since69/kra/2024/van"
alias cdnicepay="cd ~/repos/gitlab/since69/kra/2024/van/nicepay/"


