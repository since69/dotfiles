# My Dotfiles

This directory contains the dotfiles for my system


## Requirements

#nsure you have the following installed on your system

### Git

```bash

pacman -S git
```

### Stow 

```bash

pacman -S stow
```

## Installation

First, check out the dotfiles repo in your $HOME directory using git

```bash

git clone https://gitlab.com/since69/dotfiles ~/.dotfiles
cd .dotfiles
```

then use GNU stow to create symlinks


```bash

stow .
```

