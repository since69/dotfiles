# ls aliases
alias l='ls -CF'
alias ll='ls -lF'
alias la='ls -A'
alias lla='ls -alF'

# application aliases
alias vim=nvim
alias g=git

# cd aliases
alias cdr="cd ~/repos/"
alias cdgithub="cd ~/repos/github"
alias cdsince69="cd ~/repos/gitlab/since69"
alias cdsfd="cd ~/repos/gitlab/since69/fire/sfd"
alias cdsfd-db="cd ~/repos/gitlab/since69/fire/sfd/db"
alias cdsfd-src="cd ~/repos/gitlab/since69/fire/sfd/src"
alias cdsandbox="cd ~/repos/gitlab/since69/sandbox"
alias cdsandbox-c="cd ~/repos/gitlab/since69/sandbox/c"
alias cdsandbox-socket="cd ~/repos/gitlab/since69/sandbox/c/socket"
alias cdkra="cd ~/repos/gitlab/since69/kra"
alias cdnicepay="cd ~/repos/gitlab/since69/kra/nicepay"

